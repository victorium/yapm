<?php

namespace Yapm\Adapter;

use Yapm\Adapter\AdapterInterface;

class Firebird implements AdapterInterface {

    protected $cdb;

    public function __construct(array $cdb) { 
        $this->cdb = $cdb;
    }

    public function getConnectionString() {
        $cdb = $this->cdb;
        if (isset($cdb["port"]) && $cdb["port"]) {
            return "firebird:dbname={$cdb["host"]}/{$cdb["port"]}:{$cdb["name"]}";
        } else {
            return "firebird:dbname={$cdb["host"]}:{$cdb["name"]}";
        }
    }

    public function newPdoConnection() {
        $cdb = $this->cdb;
        return new \PDO($this->getConnectionString(), $cdb["user"], $cdb["pass"], $cdb["options"]);
    }
}