<?php

namespace Yapm\Adapter;

interface AdapterInterface {
    public function __construct(array $activeDbConfig);
    public function getConnectionString();
    public function newPdoConnection();
}