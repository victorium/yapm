<?php

namespace Yapm\Adapter;

use Yapm\Adapter\AdapterInterface;

class Mysql implements AdapterInterface {

    protected $cdb;
    protected $connectionString;

    public function __construct(array $cdb) { 
        $this->cdb = $cdb;
    }

    public function getConnectionString() {
        $cdb = $this->cdb;
        if ($this->connectionString === null) {
            if (isset($cdb["port"]) && $cdb["port"]) {
                $this->connectionString = "mysql:host={$cdb["host"]};port={$cdb["port"]};dbname={$cdb["name"]}";
            } else {
                $this->connectionString = "mysql:host={$cdb["host"]};dbname={$cdb["name"]}";
            }
        }
        return $this->connectionString;
    }

    public function newPdoConnection() {
        $cdb = $this->cdb;
        return new \PDO($this->getConnectionString(), $cdb["user"], $cdb["pass"], $cdb["options"]);
    }
}