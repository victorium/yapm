<?php

namespace Yapm\Adapter;

use Yapm\Adapter\AdapterInterface;
use Yapm\Mapper;

class Postgres implements AdapterInterface {

    protected $cdb;

    public function __construct(array $cdb) { 
        $this->cdb = $cdb;
    }

    public function getConnectionString() {
        $cdb = $this->cdb;
        if (isset($cdb["port"]) && $cdb["port"]) {
            return "pgsql:host={$cdb["host"]};port={$cdb["port"]};dbname={$cdb["name"]};user={$cdb["user"]};password={$cdb["pass"]}";
        } else {
            return "pgsql:host={$cdb["host"]};dbname={$cdb["name"]};user={$cdb["user"]};password={$cdb["pass"]}";
        }
    }

    public function newPdoConnection() {
        $cdb = $this->cdb;
        return new \PDO($this->getConnectionString(), null, null, $cdb["options"]);
    }

    public function sequenceName(Mapper $mapper) {
        return "{$mapper->schema->table}_{$mapper->schema->pk}_seq";
    }
}