<?php

namespace Yapm\Adapter;

use Yapm\Adapter\AdapterInterface;

class Sqlsrv implements AdapterInterface {

    protected $cdb;

    public function __construct(array $cdb) { 
        $this->cdb = $cdb;
    }

    public function getConnectionString() {
        $cdb = $this->cdb;
        if (isset($cdb["port"]) && $cdb["port"]) {
            return "sqlsrv:Server={$cdb["host"]},{$cdb["port"]};Database={$cdb["name"]}";
        } else {
            return "sqlsrv:Server={$cdb["host"]};Database={$cdb["name"]}";
        }
    }

    public function newPdoConnection() {
        $cdb = $this->cdb;
        return new \PDO($this->getConnectionString(), $cdb["user"], $cdb["pass"], $cdb["options"]);
    }
}