<?php

namespace Yapm\Adapter;

use Yapm\Adapter\AdapterInterface;

class Mssql implements AdapterInterface {

    protected $cdb;

    public function __construct(array $cdb) { 
        $this->cdb = $cdb;
    }

    public function getConnectionString() {
        $cdb = $this->cdb;
        if (isset($cdb["port"]) && $cdb["port"]) {
            return "mssql:host={$cdb["host"]},{$cdb["port"]};dbname={$cdb["name"]}";
        } else {
            return "mssql:host={$cdb["host"]};dbname={$cdb["name"]}";
        }
    }

    public function newPdoConnection() {
        $cdb = $this->cdb;
        return new \PDO($this->getConnectionString(), $cdb["user"], $cdb["pass"], $cdb["options"]);
    }
}