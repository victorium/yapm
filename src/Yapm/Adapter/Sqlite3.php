<?php

namespace Yapm\Adapter;

use Yapm\Adapter\AdapterInterface;

class Sqlite3 implements AdapterInterface {

    protected $cdb;

    public function __construct(array $cdb) { 
        $this->cdb = $cdb;
    }

    public function getConnectionString() {
        return "sqlite:{$this->cdb["name"]}";
    }

    public function newPdoConnection() {
        return new \PDO($this->getConnectionString(), null, null, $this->cdb["options"]);
    }
}