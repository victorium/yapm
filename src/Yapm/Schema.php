<?php

/*
 * Yapm/Schema.php
 *
 * Copyright: (c) 2016 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yapm;

/**
 * Class for defining schema of a model
 */
class Schema {
    /**
     * The configuration/IoC array
     * @var array
     */
    public $config;

    /**
     * The array of fields for this model schema (table columns)
     * @var array
     */
    public $fields;

    /**
     * The array of relational fields
     * @var array
     */
    public $relationFields;

    /**
    * The primary key for this model schema
    * @var string
    */
    public $pk;

    /**
     * The table for this model schema
     * @var string
     */
    public $table;
    
    /**
     * Initialize the schema
     *
     * @param array $config The configuration/IoC array
     * @param string $table The table for this model schema
     * @param string $pk The primary key for this model schema
     * @param array $fields The array of fields for this model schema (table columns)
     */
    public function __construct(array $config, $table, $pk, array $fields) {
        $this->config = $config;
        $this->table = $table;
        $this->pk = $pk;
        $this->fields = [];
        $this->relationFields = [];
        foreach ($fields as $field) {
            $this->fields[$field->propertyName] = $field;
            if ($field instanceof RelationTypeInterface) {
                $this->relationFields[$field->propertyName] = $field;
            }
        }
    }
}