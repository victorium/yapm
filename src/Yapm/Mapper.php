<?php

/*
 * Yapm/Mapper.php
 *
 * Copyright: (c) 2016 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yapm;

use Yaqb\Builder;
use Yaqb\Query;
use Yapm\Column;
use Yapm\YaqbBuilderMethodProxy;

/*
 * Mapper class for storing and retrieving objects
 */
class Mapper {

    use YaqbBuilderMethodProxy;

    const DEFAULT_ACTIVE_KEY = "db";

    /** Array of cached \PDO instance */
    protected static $connectionCache = [];

    /** Current \PDO instance */
    protected $connection;

    /** Current key in array of config */
    public $active;

    /** Yapm\Model class */
    public $modelClass;

    /** The schema for the model */
    public $schema;

    /** The instance of \Yapm\Adapter\AdapterInterface */
    protected $adapter;

    /** The instance of \Yaqb\Builder */
    protected $builder;

    /** Whether to return only one object */
    protected $isSingular;

    /** Whether to return only objects or arrays */
    protected $returnObjects;

    /** The array of results */
    public $results;

    /**
     * Initialize the mapper for a specific model
     *
     * $config = [
     *     "db" => [
     *         "adapter" => "mysql",
     *         "host" => "localhost",
     *         "port" => "3306",
     *         "name" => "database_name",
     *         "user" => "username",
     *         "pass" => "password",
     *         "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],  // array of \PDO options to apply
     *     ]
     * ];
     * new Mapper(\MyProject\MyModel::class, $config);
     * 
     * @param string $modelClass The full class name of the model
     * @param array $config The configuration/IoC
     */
    public function __construct(array $config) {
        $this->config = $config;
        $this->active = static::DEFAULT_ACTIVE_KEY;
        $this->returnObjects = true;
    }

    /**
     * Perform a raw SQL query
     *
     * @param string $sql The sql to execute
     * @param array|null $parameters The array of parameters to bind for prepared statement
     * @return array [$statement, $result]
     */
    public function raw($sql, $parameters = null) {
        $statement = $this->getConnection()->prepare($sql);
        return [$statement, $statement->execute($parameters)];
    }

    /**
     * Return the array list of the objects
     */
    public function getObjectList() {
        if (!$this->hasRun && $this->modelClass) {
            $this->hasRun = true;
            if ($this->returnObjects) {
                foreach ($this->results as $key => $row) {
                    $this->results[$key] = new $this->modelClass($row);
                }
            }
        }
        return $this->results;
    }
    
    /**
     * Run generated sql for select
     *
     * @return array|object|null The list of objects queried
     */
    public function fetch() {
        list ($statement, $status) = $this->raw($this->builder->getSql(), $this->builder->getParameters());
        $this->results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if ($this->isSingular) {
            return @$this->getObjectList()[0];
        } else {
            return $this->getObjectList();
        }
    }

    public function run() {
        list ($statement, $status) = $this->raw($this->builder->getSql(), $this->builder->getParameters());
        return $status;
    }

    /**
     * Fetch all rows that match
     *
     * $mapper->all()->run();
     * $mapper->all()->where("id", 10)->run();
     *
     * @param string|int|null $value The callback function that gets passed the builder Yaqb\Builder as argument
     * @return object|this|null The object queried
     */
    public function all($modelClass) {
        if (!class_exists($modelClass)) {
            throw new \Exception("Invalid class '{$modelClass}'");
        }
        $this->modelClass = $modelClass;
        $this->schema = $modelClass::getSchema($this->config);
        $this->builder = Builder::init()->select($this->schema->table);
        $this->results = [];
        $this->hasRun = false;
        $this->isSingular = false;
        return $this;
    }

    /**
     * Fetch single row that matches
     *
     * $mapper->get(10)->run();
     * $mapper->get()->where("id", 10)->run();
     *
     * @param string|int|null $value The callback function that gets passed the builder Yaqb\Builder as argument
     * @return object|this|null The object queried
     */
    public function get($modelClass, $value = null) {
        if (!class_exists($modelClass)) {
            throw new \Exception("Invalid class '{$modelClass}'");
        }
        $this->modelClass = $modelClass;
        $this->schema = $modelClass::getSchema($this->config);
        $this->builder = Builder::init()->select($this->schema->table);
        $this->results = [];
        $this->hasRun = false;
        $this->isSingular = true;
        if ($value !== null) {
            $this->builder->where($this->schema->pk, $value);
        }
        return $this;
    }

    /**
     * Save a model object
     *
     * $mapper->save($obj);
     *
     * @param object The model instance to save, the primary key field must be set.
     * @return $this
     */
    public function save($instance) {
        if (!is_object($instance)) {
            throw new \Exception("Invalid data sent for save");
        }
        $modelClass = get_class($instance);
        $schema = $modelClass::getSchema($this->config);
        $pk = $schema->pk;
        if (@$instance->{$pk} === null) {
            $this->builder = Builder::init()->insert($schema->table)->values($instance->toArray());
        } else {
            $this->builder = Builder::init()->update($schema->table)->values($instance->toArray())->where($pk, $instance->{$pk});
        }

        $status = $this->run();
        if (@$instance->{$pk} === null) {
            $instance->{$pk} = $this->getConnection()->lastInsertId();
        }
        return $status;
    }

    /**
     * Delete a model object or based on criteria
     *
     * $mapper->delete(Post::class, 3);
     * $mapper->delete($obj1);
     * $mapper->delete(Post::class)->where("id", 500, "<");
     * @param object|string|int|null The model instance to delete, array of model instances to delete, the primary key field must be set. If callable will do bulk delete depending on query. If null it will delete the whole table. If string or int it will delete where the primary key is that value.
     * @return $this
     */
    public function delete($instance, $value = null) {
        if (is_string($instance)) {
            $this->results = [];
            $this->hasRun = false;
            $modelClass = $this->modelClass = $instance;
            if (!class_exists($modelClass)) {
                throw new \Exception("Invalid class '{$modelClass}'");
            }
            $this->schema = $modelClass::getSchema($this->config);
            $this->builder = Builder::init()->delete($this->schema->table);

            if ($value !== null) {
                $this->builder->where($this->schema->pk, $value);
            }
            return $this;
        } elseif (is_object($instance)) {
            $modelClass = $this->modelClass = get_class($instance);
            $this->schema = $modelClass::getSchema($this->config);
            $this->builder = Builder::init()->delete($this->schema->table);
            $pk = $this->schema->pk;
            if (!@$instance->{$pk}) {
                throw new \Exception("Has no property '{$pk}' defined on instance");
            }
            $this->builder->where($pk, $instance->{$pk});
            list ($statement, $status) = $this->raw($this->builder->getSql(), $this->builder->getParameters());
            return $status;
        } else {
            throw new \Exception("\$instance or \$modelClass needed for delete");
        }
    }

    /**
     * Begin SQL transaction
     */
    public function begin() {
        $this->getConnection()->beginTransaction();
    }

    /**
     * Commit SQL transaction
     */
    public function commit() {
        return $this->getConnection()->commit();
    }

    /**
     * Rollback SQL transaction
     */
    public function rollback() {
        return $this->getConnection()->rollBack();
    }

    /**
     * Get the active configuration used by mapper
     *
     * @return array The active configuration from the array of configurations
     */
    public function getActiveConfig() {
        return $this->config[$this->active];
    }

    /**
     * The adapter the mapper is currently using that can create connections
     *
     * @return object Instance of \Yapm\AdapterInterface 
     */
    public function getAdapter() {
        if ($this->adapter === null) {
            $cdb = $this->getActiveConfig();
            $this->adapter = new $cdb["adapter"]($cdb);
        }
        return $this->adapter;
    }

    /**
     * Create a new connection to the database
     */
    public function createConnection() {
        $this->connection = $this->getAdapter()->newPdoConnection();
        static::$connectionCache[$this->getAdapter()->getConnectionString()] = $this->connection;
    }

    /**
     * Cached database connection. Cache is only to specific db/host/port and
     * does not differentiate using login credentials!
     *
     * @return object Instance of \PDO
     */
    public function getConnection() {
        if ($this->connection === null) {
            $connectionString = $this->getAdapter()->getConnectionString();
            if (array_key_exists($connectionString, static::$connectionCache)) {
                $this->connection = static::$connectionCache[$connectionString];
            } else {
                $this->createConnection();
            }
        }
        return $this->connection;
    }

    /**
     * The current builder being used by the mapper
     *
     * @return \Yaqb\Builder
     */
    public function getBuilder() {
        if ($this->builder === null) {
            $this->builder = Builder::init();
        }
        return $this->builder;
    }
}
