<?php

namespace Yapm\Column;

use Yapm\Column\NumericType;

class AutoIdType extends NumericType {
    public $columnName;
    public $propertyName;

    public function __construct($name) {
        $this->columnName = $this->propertyName = $name;
    }
}