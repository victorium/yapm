<?php

namespace Yapm\Column;

use Yapm\Column\RelationTypeInterface;

class ManyToManyType implements RelationTypeInterface {
    public $columnName;
    public $propertyName;
    public $relatedClassName;

    public function __construct($name, $relatedClassName) {
        $this->propertyName = $name;
        $this->columnName = null;
        $this->relatedClassName = $relatedClassName;
    }

    public function fromDbColumn($model, array $rows) {
        
    }

    public function toDbColumn ($model) {
        return null;
    }

    public function addToInstance($instance, $mapper, $dataCache = null) {
        
    }
}