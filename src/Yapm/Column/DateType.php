<?php

namespace Yapm\Column;

use Yapm\Column\TypeInterface;

class DateType implements TypeInterface {
    public $columnName;
    public $propertyName;
    public $dbFormat = "Y-m-d";
    public $dateFormat = "Y-m-d";

    public function __construct($name) {
        $this->columnName = $this->propertyName = $name;
    }

    public function fromDbColumn($model, array $row) {
        $value = @$row[$this->columnName];
        if ($value === "") {
            $value = null;
        }
        if ($value instanceof \DateTime) {
            //continue
        } elseif ($value !== null) {
            $value = \DateTime::createFromFormat($this->getDbFormat(), $value);
        }
        $model->{$this->propertyName} = $value;
    }

    public function toDbColumn($model) {
        $value = $model->{$this->propertyName};
        if ($value instanceof \DateTime) {
            $value = $value->format($this->dbFormat);
        } else {
            if ($value === "") {
                $value = null;
            }
            if ($value !== null) {
                $date =  \DateTime::createFromFormat($this->dateFormat, $value);
                if ($date !== false) {
                    $value = $date->format($this->dbFormat);
                }
            }
        }
        return $value;
    }
}