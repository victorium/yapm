<?php

namespace Yapm\Column;

use Yapm\Column\TypeInterface;

class StringType implements TypeInterface {
    public $columnName;
    public $propertyName;

    public function __construct($name) {
        $this->columnName = $this->propertyName = $name;
    }

    public function fromDbColumn($model, array $row) {
        $model->{$this->propertyName} = @$row[$this->columnName];
    }

    public function toDbColumn($model) {
        return $model->{$this->propertyName};
    }
}