<?php

namespace Yapm\Column;

use Yapm\Column\DateType;

class DateTimeType extends DateType {
    public $dbFormat = "Y-m-d H:i:s";
    public $dateFormat = "Y-m-d H:i:s";
}