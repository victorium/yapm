<?php

namespace Yapm\Column;

interface TypeInterface {
    public function fromDbColumn($model, array $rows);
    public function toDbColumn($model);
}