<?php

namespace Yapm\Column;

use Yapm\Column\RelationTypeInterface;

class ForeignKeyType implements RelationTypeInterface {
    public $columnName;
    public $propertyName;
    public $relatedClassName;

    public function __construct($name, $relatedClassName) {
        $this->propertyName = $name;
        $this->columnName = "{$name}_id";
        $this->relatedClassName = $relatedClassName;
    }

    public function fromDbColumn($model, array $row) {
        $columnValue = @$row[$this->columnName];
        $propertyValue = @$row[$this->propertyName];

        if ($propertyValue) {
            $model->{$this->columnName} = $propertyValue->{$model->getSchema([])->pk};
            $model->{$this->propertyName} = $propertyValue;
        } else {
            $model->{$this->columnName} = $columnValue;
            $model->{$this->propertyName} = null;
        }
    }

    public function addToInstance($instance, $mapper, $dataCache = null) {
        $relatedClassName = $this->relatedClassName;
        $relatedPk = $relatedClassName::getSchema([])->pk;
        $pkValue = @$instance->{$this->columnName};

        if ($relatedPk && $pkValue) {
            $relatedInstance = $mapper->get($relatedClassName)->where($relatedPk, $pkValue)->run();
            $instance->{$this->propertyName} = $relatedInstance;
        }
    }

    public function toDbColumn($model) {
        $value = $model->{$this->columnName};
        $relationInstance = @$model->{$this->propertyName};
        if ($relationInstance !== null) {
            $relationInstanceModelClass = get_class($relationInstance);
            $relationInstanceSchema = $relationInstanceModelClass::getSchema([]);
            $relationInstancePk = $relationInstanceSchema->pk;
            if (
                    $relationInstance !== null &&
                    @$relationInstance->{$relationInstancePk} !== null &&
                    @$model->{$this->columnName} === null
                ) {
                $value = $relationInstance->{$relationInstancePk};
                $model->{$this->columnName} = $value;
            }
        }
        return $value;
    }
}