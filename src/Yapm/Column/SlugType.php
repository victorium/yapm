<?php

namespace Yapm\Column;

use Yapm\Column\StringType;

class SlugType extends StringType {
    public $fromColumnName;

    public function __construct($name, $fromColumnName) {
        $this->columnName = $this->propertyName = $name;
        $this->fromColumnName = $fromColumnName;
    }

    public function toDbColumn($model) {
        $oldValue = $model->{$this->propertyName};
        if ($oldValue === null) {
            $field = $model->getSchema([])->fields[$this->fromColumnName];
            $value = $field->toDbColumn($model);
            if ($value !== null) {
                $value = $this->slugFor($value);
                $model->{$this->propertyName} = $value;
            }
        } else {
            $value = $oldValue;
        }
        return $value;
    }

    public function slugFor($value) {
        return preg_replace("/\W+/", "-", trim($value));
    }
}