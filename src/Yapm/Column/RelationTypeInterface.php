<?php

namespace Yapm\Column;

use Yapm\Column\TypeInterface;

interface RelationTypeInterface extends TypeInterface {
    public function addToInstance($instance, $mapper, $dataCache = null);
}