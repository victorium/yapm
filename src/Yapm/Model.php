<?php

/*
 * Yapm/Model.php
 *
 * Copyright: (c) 2016 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yapm;

/**
 * Class that all models must inherit from
 */
abstract class Model implements \JsonSerializable {

    protected static $schema;

    abstract protected static function defineSchema($config);

    /**
     * Gets the schema defined in $this->defineSchema() function. This function is mainly called by the Mapper
     *
     * @param array $config The configurations/IoC array
     * @return \Yapm\Schema The defined schema
     */
    public static function getSchema(array $config) {
        if (@static::$schema[static::class] === null) {
            static::$schema[static::class] = static::defineSchema($config);
        }
        return static::$schema[static::class];
    }

    /**
     * Construct a model
     *
     * @param array|null $args The initial properties for this model
     */
    public function __construct(array $args = null) {
        $args = $args === null ? [] : $args;
        $this->populate($args);
    }

    /**
     * Populate this model instance properties from an array
     *
     * @param array $args The map of key => value pair where eaach key will become an object property
     */
    public function populate(array $args) {
        $fields = static::getSchema([])->fields;

        foreach ($args as $name => $value) {
            if (!isset($fields[$name])) {
                $this->{$name} = $value;
            }
        }

        foreach ($fields as $name => $field) {
            $field->fromDbColumn($this, $args);
        }
        return $this;
    }

    /**
     * This mode as an array
     *
     * @return array This model
     */
    public function toArray() {
        $values = [];
        foreach ($this->getSchema([])->fields as $field) {
            if ($field->columnName) {
                $values[$field->columnName] = $field->toDbColumn($this);
            }
        }
        return $values;
    }

    public function jsonSerialize() {
        return $this->toArray();
    }

    public function __toString() {
        return var_export($this, true);
    }
}
