<?php

namespace Yapm;

use Yaqb\Builder;

trait YaqbBuilderMethodProxy {
    public function update($modelClass) {
        if (!class_exists($modelClass)) {
			throw new \Exception("Invalid class '{$modelClass}'");
		}
		$this->modelClass = $modelClass;
        $this->schema = $modelClass::getSchema($this->config);
        $this->builder = Builder::init()->update($this->schema->table);
        return $this;
	}

    public function values($data) {
		$this->getBuilder()->values($data);
        return $this;
	}

    public function action($action) {
		$this->getBuilder()->action($action);
        return $this;
	}

	public function column($column, $alias = null) {
		$this->getBuilder()->column($column, $alias);
		return $this;
	}

	public function subQuery($sql, $alias = null) {
		$this->getBuilder()->subQuery($sql, $alias);
		return $this;
	}

	public function limit($limit) {
		$this->getBuilder()->limit($limit);
		return $this;
	}

	public function offset($offset) {
		$this->getBuilder()->offset($offset);
		return $this;
	}

	public function where($key, $value, $comparison = null) {
		$this->getBuilder()->where($key, $value, $comparison);
		return $this;
    }

    public function whereRaw($expression) {
		$this->getBuilder()->whereRaw($expression);
		return $this;
	}

	public function subWhere() {
		$this->getBuilder()->subWhere();
		return $this;
	}

	public function subWhereEnd() {
		$this->getBuilder()->subWhereEnd();
		return $this;
	}

	public function groupBy($expression) {
		$this->getBuilder()->groupBy($expression);
		return $this;
	}

	public function having($expression) {
		$this->getBuilder()->having($expression);
		return $this;
	}

	public function orderBy($expression) {
		$this->getBuilder()->orderBy($expression);
		return $this;
	}

	public function orCondition() {
		$this->getBuilder()->orCondition();
		return $this;
	}

	public function andCondition() {
		$this->getBuilder()->andCondition();
		return $this;
	}

	public function addParameters($parameters) {
		$this->getBuilder()->addParameters($parameters);
		return $this;
	}

	public function innerJoin($table, $expression) {
		$this->getBuilder()->innerJoin($table, $expression);
		return $this;
	}
    
	public function leftJoin($table, $expression) {
		$this->getBuilder()->leftJoin($table, $expression);
		return $this;
	}

	public function leftOuterJoin($table, $expression) {
		$this->getBuilder()->leftOuterJoin($table, $expression);
		return $this;
	}
}