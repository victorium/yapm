<?php

define("PATH_TO_YAPM_SRC", dirname(__DIR__));

require PATH_TO_YAPM_SRC . "/bootstrap.php";

use Yapm\Adapter\Mysql;
use Yapm\Adapter\Sqlite3;
use Yapm\Mapper;
use Yapm\Model;
use Yapm\Schema;

class Post extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "post", "id", [
            "id",
            "slug",
            "title",
            "author_id",
            "content",
        ]);
    }
}

$config = [
    "db1" => [
        "adapter" => Sqlite3::class,
        "name" => ":memory:",
        "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
    ],
    "db2" => [
        "adapter" => Mysql::class,
        "name" => "db2",
        "host" => "localhost",
        "user" => "username",
        "pass" => "password",
        "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
    ]
];

$mapper1 = Mapper::domain(Post::class, $config)->setActive("db1");
$mapper2 = Mapper::domain(Post::class, $config)->setActive("db2");

// stupid way to copy a table between databases ;-)
foreach ($mapper1->all() as $post) {
    $mapper2->insert($post);
}