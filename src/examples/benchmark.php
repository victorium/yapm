<?php

define("PATH_TO_YAPM_SRC", dirname(__DIR__));

require PATH_TO_YAPM_SRC . "/bootstrap.php";

use Yapm\Adapter\Sqlite3;
use Yapm\Column;
use Yapm\Mapper;
use Yapm\Model;
use Yapm\Schema;

class Post extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "post", "id", [
            new Column\AutoIdType("id"),
            new Column\StringType("title"),
            new Column\SlugType("slug", "title"),
            new Column\NumericType("author_id"),
            new Column\StringType("content"),
        ]);
    }
}

/**
 * Sample results:
 *
 * PHP 5.6.11:
 * Raw  select 100,000 times took: 0.43180012702942
 * YAPM select 100,000 times took: 0.91498780250549
 * 
 * Raw  insert 10,000 times took: 0.034385919570923
 * YAPM insert 10,000 times took: 0.10805320739746
 * 
 * PHP 7.0.0:
 * Raw  select 100,000 times took: 1.5507581233978
 * YAPM select 100,000 times took: 2.8913421630859
 * 
 * Raw  insert 10,000 times took: 0.11746382713318
 * YAPM insert 10,000 times took: 0.33016395568848
 */
function main() {
    $config = [
        "db" => [
            "adapter" => Sqlite3::class,
            "name" => ":memory:",
            "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
        ]
    ];

    list($mapper, $db) = initializeDb($config);

    $duration1 = rawSelect($db);
    $duration2 = yapmSelect($mapper);
    echo "Raw  select 100,000 times took: {$duration1}\n" .
         "YAPM select 100,000 times took: {$duration2}\n\n";

    $duration1 = rawInsert($db);
    $duration2 = yapmInsert($mapper);
    echo "Raw  insert 10,000 times took: {$duration1}\n" .
         "YAPM insert 10,000 times took: {$duration2}\n\n";
}

function initializeDb($config) {
    $mapper = new Mapper($config);
    $db = $mapper->getConnection();
    $db->exec("
    DROP TABLE IF EXISTS post;
    CREATE TABLE post (
        id INTEGER PRIMARY KEY,
        slug VARCHAR (200),
        title VARCHAR (200),
        author_id INTEGER,
        content TEXT
    );
    INSERT INTO post VALUES
        (null, 'the-funny', 'The funny', 1, 'The funniest things are funny'),
        (null, 'the-sad', 'The sad', 2, 'The saddest things make use cry');
    ");
    return [$mapper, $db];
}

function rawSelect($db) {
    $start = microtime(true);
    for ($i=0; $i<100000; $i++) {
        $sql =  "SELECT * FROM post";
        $statement = $db->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
    }
    $end = microtime(true);
    return $end - $start;
}

function yapmSelect($mapper) {
    $start = microtime(true);
    for ($i=0; $i<100000; $i++) {
        $rows = $mapper->all(Post::class)->fetch();
    }
    $end = microtime(true);
    return $end - $start;
}

function rawInsert($db) {
    $start = microtime(true);
    for ($i=0; $i<10000; $i++) {
        $sql =  "INSERT INTO post VALUES (null, :slug, :title, :author_id, :content)";
        $statement = $db->prepare($sql);
        $statement->execute([
            "slug" => "the-funny",
            "title" => "The funny",
            "author_id" => 1,
            "content" => "The funniest things are funny"
        ]);
        $rows = $statement->fetchAll();
    }
    $end = microtime(true);
    return $end - $start;
}

function yapmInsert($mapper) {
    $start = microtime(true);
    for ($i=0; $i<10000; $i++) {
        $post = new Post;
        $post->title = "The start";
        $post->slug = "the-start";
        $post->content = "All the things start with the beginning";
        $post->author_id = 1;
        $rows = $mapper->save($post);
    }
    $end = microtime(true);
    return $end - $start;
}

main();