<?php

define("PATH_TO_YAPM_SRC", dirname(__DIR__));

require PATH_TO_YAPM_SRC . "/bootstrap.php";

use Yapm\Adapter\Sqlite3;
use Yapm\Column;
use Yapm\Mapper;
use Yapm\Model;
use Yapm\Schema;

class Contribution extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "contribution", "id", [
            new Column\AutoIdType("id"),
            new Column\ForeignKeyType("author", Author::class),
            new Column\ForeignKeyType("post", Post::class),
        ]);
    }
}

class Author extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "author", "id", [
            new Column\AutoIdType("id"),
            new Column\StringType("title"),
            new Column\StringType("name"),
            //new Column\OneToManyType("posts", Post::class),
            new Column\ManyToManyType("contributions", Contribution::class),
        ]);
    }
}

class Post extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "post", "id", [
            new Column\AutoIdType("id"),
            new Column\StringType("title"),
            new Column\SlugType("slug", "title"),
            new Column\ForeignKeyType("author", Author::class),
            new Column\StringType("content"),
            new Column\ManyToManyType("contributors", Contribution::class),
        ]);
    }
}

$mapper = null;

function main() {
    setup();
    insertPost();
    var_dump(selectPost());
    insertPosts();
    var_dump(selectPosts());
}

function getConfig() {
    return [
        "db" => [
            "adapter" => Sqlite3::class,
            "name" => ":memory:",
            "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
        ]
    ];
}

function getMapper() {
    global $mapper;
    if ($mapper === null) {
        $mapper = new Mapper(getConfig());
    }
    return $mapper;
}

function setUp() {
    getMapper()->getConnection()->exec("
    DROP TABLE IF EXISTS author;
    CREATE TABLE author (
        id INTEGER PRIMARY KEY,
        title VARCHAR (200),
        name VARCHAR (200)
    );
    DROP TABLE IF EXISTS contribution;
    CREATE TABLE contribution (
        id INTEGER PRIMARY KEY,
        post_id INTEGER,
        author_id INTEGER
    );
    DROP TABLE IF EXISTS post;
    CREATE TABLE post (
        id INTEGER PRIMARY KEY,
        slug VARCHAR (200),
        title VARCHAR (200),
        author_id INTEGER,
        content TEXT
    );");
}

function insertPost() {
    $post = new Post;
    $post->title = "The basic";
    $post->content = "All the basic things build into big things";
    $author = new Author(["name" => "Philosopher" ,"title" => "Professor"]);
    getMapper()->save($author);
    $post->author = $author;
    getMapper()->save($post);
}

function selectPost() {
    return getMapper()->get(Post::class, 1)->fetch();
}

function insertPosts() {
    $author1 = getMapper()->get(Author::class, 1)->fetch();
    $author2 = new Author(["name" => "X" ,"title" => "Professor"]);
    getMapper()->save($author2);
    $posts = [
        new Post(["title" => "The Start", "author" => $author1, "slug" => "the-start", "content" => "All the things start with the beginning"]),
        new Post(["title" => "The End", "author" => $author2, "slug" => "the-end", "content" => "All the things endd with the ending"]),
    ];
    foreach ($posts as $post) {
        getMapper()->save($post);
    }
}

function selectPosts() {
    return getMapper()->all(Post::class)->fetch();
}

main();