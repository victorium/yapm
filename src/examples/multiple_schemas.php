<?php

define("PATH_TO_YAPM_SRC", dirname(__DIR__));

require PATH_TO_YAPM_SRC . "/bootstrap.php";

use Yapm\Adapter\Sqlite3;
use Yapm\Model;
use Yapm\Schema;

class AppPrefixSchema extends Schema {
    protected $prefix = "app_";

    function __construct(array $config, $table, $pk, array $fields) {
        parent::__construct($config, $this->prefix . $table, $pk, $fields);
    }
}

class Post extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "post", "id", [
            "id",
            "slug",
            "title",
            "author_id",
            "content",
        ]);
    }
}

class PrefixedAuthor extends Model {
    protected static function defineSchema($config) {
        return new AppPrefixSchema($config, "author", "id", [
            "id",
            "salutation",
            "name",
        ]);
    }
}

$post = new Post;
$prefixedAuthor = new PrefixedAuthor;

$config = [
    "db" => [
        "adapter" => Sqlite3::class,
        "name" => ":memory:",
        "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
    ]
];

var_dump($post->getSchema($config)->table);
var_dump($prefixedAuthor->getSchema($config)->table);
