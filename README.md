# Yet Another Persistance Mapper (YAPM) for PHP

# Introduction

YAPM is a very simple, stupid implementation framework agnostic ORM. It uses PDO database abstractions and currently supports MySQL, PostgreSQL, Sqlite3, Firebird and MS SQL databases. It uses the mysql, pgsql, sqlite, firebird, sqlsrv, mssql PDO drivers.

## Licence

All source files in YAPM are released subject to the terms of the MIT license, as written in the included LICENSE.txt file.

## Installation

### Composer

YAPM can be installed with composer (http://getcomposer.org/).

1. Install composer:

	```
	$ curl -s https://getcomposer.org/installer | php
	```

2. Require YAPM as a dependency using composer:

	```
	$ php composer.phar require victorium/yapm
	```

## Getting Started

This is a simple example on getting started with YAPM:

	<?php

    define("PATH_TO_YAPM_SRC", dirname(__DIR__));

    require PATH_TO_YAPM_SRC . "/bootstrap.php";

    use Yapm\Adapter\Sqlite3;
    use Yapm\Column;
    use Yapm\Mapper;
    use Yapm\Model;
    use Yapm\Schema;
    
    class Contribution extends Model {
        protected static function defineSchema($config) {
            return new Schema($config, "contribution", "id", [
                new Column\AutoIdType("id"),
                new Column\ForeignKeyType("author", Author::class),
                new Column\ForeignKeyType("post", Post::class),
            ]);
        }
    }
    
    class Author extends Model {
        protected static function defineSchema($config) {
            return new Schema($config, "author", "id", [
                new Column\AutoIdType("id"),
                new Column\StringType("title"),
                new Column\StringType("name"),
                //new Column\OneToManyType("posts", Post::class),
                new Column\ManyToManyType("contributions", Contribution::class),
            ]);
        }
    }
    
    class Post extends Model {
        protected static function defineSchema($config) {
            return new Schema($config, "post", "id", [
                new Column\AutoIdType("id"),
                new Column\StringType("title"),
                new Column\SlugType("slug", "title"),
                new Column\ForeignKeyType("author", Author::class),
                new Column\StringType("content"),
                new Column\ManyToManyType("contributors", Contribution::class),
            ]);
        }
    }
    function getConfig() {
        return [
            "db" => [
                "adapter" => "sqlite",
                "name" => ":memory:",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ]
        ];
    }
    
    $mapper = new Mapper(getConfig());

## Features

These code snippets demonstrate basic CRUD functionality. Queries can be flexibly built using builder defined in YAQB where necessary.

### Querying

    <?php
    
    // fetching multiple objects
    $posts = $mapper->all(Post::class)->fetch();
    $posts = $mapper->all(Post::class)->where("author_id", 1)->fetch();

    // fetching one object
    $post = $mapper->get(Post::class, 1)->fetch();
    $post = $mapper->get(Post::class)->where("author_id", 1)->where("slug", "the-funny")->fetch();


### Inserting

    <?php
    
    // inserting one object
    $post = new Post;
    $post->title = "The start";
    $post->slug = "the-start";
    $post->content = "All the things start with the beginning";
    $post->author_id = 9;
    $mapper->save($post);

    // inserting multiple objects
    $posts = [
        new Post(["title" => "The Start", "author_id" => 9,
            "slug" => "the-start", "content" => "All the things start with the beginning"
        ]),
        new Post(["title" => "The End", "author_id" => 9,
            "slug" => "the-end", "content" => "All the things end with the ending"
        ]),
    ];
    array_map(function ($post) use ($mapper) { return $mapper->save($post); }, $posts);

### Updating

    <?php

    // updating one object
    $post = $mapper->get(Post::class, 1)->fetch();
    $post->slug = "new-slug";
    $mapper->save($post);

    // updating multiple objects with condition (bulk update)
    $mapper->update(Post::class)->values(["author_id" => 404])->where("slug", "the-start")->run();

### Deleting

    <?php
    
    // deleting one object
    $post = $mapper->get(Post::class, 1)->fetch();
    $mapper->delete($post);
    
    // alternatives that don't require first fetching the object
    $mapper->delete(Post::class, 1)->run();
    $mapper->delete(Post::class)->where("id", 1)->run();
    
    // deleting multiple objects
    $posts = $mapper->all(Post::class)->fetch();
    array_map(function ($post) use ($mapper) { return $mapper->delete($post); }, $posts);

### Transations

    <?php
    
    $post = null;
    
    try {
        $mapper->begin();
        $mapper->delete($post);
        $mapper->commit();
    } catch (\Exception $err) {
        $mapper->rollback();
    }
