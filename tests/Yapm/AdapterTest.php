<?php

use tests\Models\Post;
use Yapm\Adapter\Firebird;
use Yapm\Adapter\Mssql;
use Yapm\Adapter\Mysql;
use Yapm\Adapter\Postgres;
use Yapm\Adapter\Sqlite3;
use Yapm\Adapter\Sqlsrv;
use Yapm\Mapper;

class AdapterTest extends PHPUnit_Framework_TestCase {
    
    public function getConfig() {
        return [
            "firebird" => [
                "adapter" => Firebird::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "port" => 3306,
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "firebird_plain" => [
                "adapter" => Firebird::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "mysql" => [
                "adapter" => Mysql::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "port" => 3306,
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "mysql_plain" => [
                "adapter" => Mysql::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "mssql" => [
                "adapter" => Mssql::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "port" => 123,
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "mssql_plain" => [
                "adapter" => Mssql::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "sqlsrv" => [
                "adapter" => Sqlsrv::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "port" => 123,
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "sqlsrv_plain" => [
                "adapter" => Sqlsrv::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "pgsql" => [
                "adapter" => Postgres::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "port" => 5432,
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "pgsql_plain" => [
                "adapter" => Postgres::class,
                "user" => "lol",
                "pass" => "lol",
                "name" => "lol",
                "host" => "localhost",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
            "sqlite3" => [
                "adapter" => Sqlite3::class,
                "name" => ":memory:",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ],
        ];
    }

    public function testNoDriversHost() {
        foreach ($this->getConfig() as $key => $config) {
            if (!in_array($key, ["mysql_plain", "mysql", "pgsql", "pgsql_plain", "sqlite3"])) {
                try {
                    $adapter = new $config["adapter"]($config);
                    $adapter->newPdoConnection();
                    $this->assertEquals(1, 2, "Not supposed to get here");
                } catch (\PDOException $err) {
                    $this->assertEquals("could not find driver", $err->getMessage());
                }
            }
        }
    }

    public function testMysql() {
        $adapter = new Mysql($this->getConfig()["mysql"]);
        try {
            $adapter->newPdoConnection();
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\PDOException $err) {
            $this->assertEquals("SQLSTATE[28000] [1045] Access denied for user 'lol'@'localhost' (using password: YES)", $err->getMessage());
        }
    }

    public function testMysqlNoPort() {
        $adapter = new Mysql($this->getConfig()["mysql_plain"]);
        try {
            $adapter->newPdoConnection();
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\PDOException $err) {
            $this->assertEquals("SQLSTATE[28000] [1045] Access denied for user 'lol'@'localhost' (using password: YES)", $err->getMessage());
        }
    }

    public function testPostgres() {
        $adapter = new Postgres($this->getConfig()["pgsql"]);
        try {
            $adapter->newPdoConnection();
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\PDOException $err) {
            $this->assertEquals('SQLSTATE[08006] [7] FATAL:  role "lol" does not exist', $err->getMessage());
        }
    }

    public function testPostgresNoPort() {
        $adapter = new Postgres($this->getConfig()["pgsql_plain"]);
        try {
            $adapter->newPdoConnection();
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\PDOException $err) {
            $this->assertEquals('SQLSTATE[08006] [7] FATAL:  role "lol" does not exist', $err->getMessage());
        }
    }

    public function testPostgresSequence() {
        $mapper = new Mapper($this->getConfig());
        $mapper->active = "pgsql";
        $mapper->schema = Post::getSchema($this->getConfig());
        $adapter = $mapper->getAdapter();
        $this->assertEquals("post_id_seq", $adapter->sequenceName($mapper));
    }

    public function testSqlite3() {
        $adapter = new Sqlite3($this->getConfig()["sqlite3"]);
        $adapter->newPdoConnection();
    }
}