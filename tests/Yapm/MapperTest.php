<?php

use tests\Models\Author;
use tests\Models\Contribution;
use tests\Models\Post;
use Yapm\Adapter\Sqlite3;
use Yapm\Mapper;
use Yaqb\Builder;

class MapperTest extends PHPUnit_Framework_TestCase {

    public function getConfig() {
        return [
            "db" => [
                "adapter" => Sqlite3::class,
                "name" => ":memory:",
                "options" => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION],
            ]
        ];
    }

    public function setUp() {
        $mapper = new Mapper($this->getConfig());
        $db = $mapper->getConnection();
        $db->exec("
        DROP TABLE IF EXISTS author;
    CREATE TABLE author (
        id INTEGER PRIMARY KEY,
        title VARCHAR (200),
        name VARCHAR (200)
    );
    DROP TABLE IF EXISTS contribution;
    CREATE TABLE contribution (
        id INTEGER PRIMARY KEY,
        post_id INTEGER,
        author_id INTEGER
    );
    DROP TABLE IF EXISTS post;
        CREATE TABLE post (
            id INTEGER PRIMARY KEY,
            slug VARCHAR (200),
            title VARCHAR (200),
            author_id INTEGER,
            content TEXT
        );
        INSERT INTO post VALUES
            (null, 'the-funny', 'The funny', 1, 'The funniest things are funny'),
            (null, 'the-sad', 'The sad', 2, 'The saddest things make use cry');
        ");
    }

    public function testFilterAllPosts() {
        $mapper = new Mapper($this->getConfig());
        $posts = $mapper->all(Post::class)->fetch();

        $this->assertEquals(count($posts), 2);

        list($post1, $post2) = $posts;

        $this->assertEquals($post1->id, 1);
        $this->assertEquals($post1->title, "The funny");
        $this->assertEquals($post1->slug, "the-funny");
        $this->assertEquals($post1->author_id, 1);
        $this->assertEquals($post1->content, "The funniest things are funny");

        $this->assertEquals($post2->id, 2);
        $this->assertEquals($post2->title, "The sad");
        $this->assertEquals($post2->slug, "the-sad");
        $this->assertEquals($post2->author_id, 2);
        $this->assertEquals($post2->content, "The saddest things make use cry");
    }

    public function testFilterAllPostsQueried() {
        $mapper = new Mapper($this->getConfig());
        $posts = $mapper->all(Post::class)->where("author_id", 1)->fetch();

        $this->assertEquals(count($posts), 1);

        $post = $posts[0];

        $this->assertEquals($post->id, 1);
        $this->assertEquals($post->title, "The funny");
        $this->assertEquals($post->slug, "the-funny");
        $this->assertEquals($post->author_id, 1);
        $this->assertEquals($post->content, "The funniest things are funny");
    }

    public function testFilterOnePost() {
        $mapper = new Mapper($this->getConfig());
        $post = $mapper->get(Post::class, 1)->fetch();

        $this->assertEquals($post->id, 1);
        $this->assertEquals($post->title, "The funny");
        $this->assertEquals($post->slug, "the-funny");
        $this->assertEquals($post->author_id, 1);
        $this->assertEquals($post->content, "The funniest things are funny");
    }

    public function testFilterOnePostNonExistant() {
        $mapper = new Mapper($this->getConfig());
        $post = $mapper->get(Post::class, 100)->fetch();
        $this->assertEquals($post, null);
    }

    public function testFilterOnePostQueried() {
        $mapper = new Mapper($this->getConfig());
        $post = $mapper->get(Post::class)->where("author_id", 1)->where("slug", "the-funny")->fetch();

        $this->assertEquals($post->id, 1);
        $this->assertEquals($post->title, "The funny");
        $this->assertEquals($post->slug, "the-funny");
        $this->assertEquals($post->author_id, 1);
        $this->assertEquals($post->content, "The funniest things are funny");
    }

    public function testInsertAPost() {
        $mapper = new Mapper($this->getConfig());
        $post = new Post;
        $post->title = "The start";
        $post->slug = "the-start";
        $post->content = "All the things start with the beginning";
        $post->author_id = 9;
        $mapper->save($post);
        $this->assertEquals($post->id, 3);
    }

    public function testInsertAnArray() {
        $mapper = new Mapper($this->getConfig());
        $post = new Post(["title" => "The End", "author_id" => 9, "slug" => "the-end", "content" => "All the things endd with the ending"]);
        $mapper->save($post);
        $this->assertEquals($post->id, 3);
    }

    public function testInsertError() {
        try {
            $mapper = new Mapper($this->getConfig());
            $post = "LOL";
            $mapper->save($post);
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\Exception $err) {
            $this->assertEquals($err->getMessage(), "Invalid data sent for save");
        }
    }

    public function testInsertManyPosts() {
        $mapper = new Mapper($this->getConfig());
        $posts = [
            new Post(["title" => "The Start", "author_id" => 9, "slug" => "the-start", "content" => "All the things start with the beginning"]),
            new Post(["title" => "The End", "author_id" => 9, "slug" => "the-end", "content" => "All the things endd with the ending"]),
        ];
        array_map(function ($post) use ($mapper) { return $mapper->save($post); }, $posts);
        $this->assertEquals(count($mapper->all(Post::class)->fetch()), 4);
    }

    public function testUpdatePost() {
        $mapper = new Mapper($this->getConfig());
        $post = $mapper->get(Post::class, 1)->fetch();

        $post->slug = "new-slug";
        $mapper->save($post);

    }

    public function testUpdateArrayBad() {
        $mapper = new Mapper($this->getConfig());
        $post = ["title" => "The End", "author_id" => 9, "slug" => "the-end", "content" => "All the things endd with the ending"];

        try {
            $mapper->save($post);
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\Exception $err) {
            $this->assertEquals($err->getMessage(), "Invalid data sent for save");
        }
    }

    public function testBulkUpdate() {
        $mapper = new Mapper($this->getConfig());
        $mapper->update(Post::class)->values(["author_id" => 404])->where("slug", "the-start")->run();
        $posts = $mapper->all(Post::class)->fetch();
    }

    public function testDeletePk() {
        $mapper = new Mapper($this->getConfig());
        $mapper->delete(Post::class, 1)->run();
        $this->assertEquals(count($mapper->all(Post::class)), 1);
    }

    public function testDeleteQueried() {
        $mapper = new Mapper($this->getConfig());
        $mapper->delete(Post::class)->where("id", 1)->run();
        $this->assertEquals(count($mapper->all(Post::class)), 1);
    }

    public function testDeleteObject() {
        $mapper = new Mapper($this->getConfig());

        $post = $mapper->get(Post::class, 1)->fetch();
        $mapper->delete($post);

        $this->assertEquals(count($mapper->all(Post::class)->fetch()), 1);
    }

    public function testDeleteBadObject() {
        $mapper = new Mapper($this->getConfig());

        $post = new Post(["title" => "The End", "author_id" => 9, "slug" => "the-end", "content" => "All the things endd with the ending"]);

        try {
            $mapper->delete($post);
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\Exception $err) {
            $this->assertEquals($err->getMessage(), "Has no property 'id' defined on instance");
            $this->assertEquals(count($mapper->all(Post::class)->fetch()), 2);
        }
    }

    public function testDeleteObjectList() {
        $mapper = new Mapper($this->getConfig());
        $posts = $mapper->all(Post::class)->fetch();
        array_map(function ($post) use ($mapper) { return $mapper->delete($post); }, $posts);
        $this->assertEquals(count($mapper->all(Post::class)->fetch()), 0);
    }

    public function testDeleteObjectBadList() {
        $mapper = new Mapper($this->getConfig());

        $posts = [
            new Post(["title" => "The Start", "author_id" => 9, "slug" => "the-start", "content" => "All the things start with the beginning"]),
            new Post(["title" => "The End", "author_id" => 9, "slug" => "the-end", "content" => "All the things endd with the ending"]),
        ];

        try {
            $mapper->delete($posts)->run();
            $this->assertEquals(1, 2, "Not supposed to get here");
        } catch (\Exception $err) {
            $this->assertEquals($err->getMessage(), "\$instance or \$modelClass needed for delete");
            $this->assertEquals(count($mapper->all(Post::class)->fetch()), 2);
        }
    }

    public function testCommit() {
        $mapper = new Mapper($this->getConfig());

        $mapper->begin();
        $mapper->delete(Post::class)->run();
        $mapper->commit();

        $this->assertEquals(count($mapper->all(Post::class)->fetch()), 0);
    }

    public function testRollback() {
        $mapper = new Mapper($this->getConfig());
        $mapper->begin();
        $mapper->delete(Post::class)->run();
        $mapper->rollback();
        $this->assertEquals(count($mapper->all(Post::class)->fetch()), 2);
    }

    public function testBuilderProxy() {
        $mapper = new Mapper($this->getConfig());

        $posts = $mapper->all(Post::class)->action("DISTINCT")->column("id")->limit(1)->offset(1)->fetch();
        $this->assertEquals(1, count($posts));
        $post = $posts[0];

        $this->assertEquals($post->id, 2);
        $this->assertEquals($post->title, null);
        $this->assertEquals($post->slug, null);
        $this->assertEquals($post->author_id, null);
        $this->assertEquals($post->content, null);
    }

    public function testBuilderProxy2() {
        $mapper = new Mapper($this->getConfig());

        $posts = $mapper->all(Post::class)->column("SUM(author_id)", "a_sum")->groupBy("id")->having("a_sum>=2")->fetch();
        $this->assertEquals(1, count($posts));
        $post = $posts[0];

        $this->assertEquals($post->a_sum, 2);
        $this->assertEquals($post->id, null);
        $this->assertEquals($post->title, null);
        $this->assertEquals($post->slug, null);
        $this->assertEquals($post->author_id, null);
        $this->assertEquals($post->content, null);
    }

    public function testBuilderProxy3() {
        $mapper = new Mapper($this->getConfig());

        $subBuilder = Builder::init("sub")->select()->column("1")->limit(1);
        $posts = $mapper->all(Post::class)->subQuery($subBuilder, "one")
            ->subWhere()
                ->where("id", 1, ">")
                ->orCondition()
                ->where("author_id", "1")
            ->subWhereEnd()
            ->fetch();

        $this->assertEquals(2, count($posts));

        list($post1, $post2) = $posts;

        $this->assertEquals($post1->one, 1);
        $this->assertEquals($post1->id, null);
        $this->assertEquals($post1->title, null);
        $this->assertEquals($post1->slug, null);
        $this->assertEquals($post1->author_id, null);
        $this->assertEquals($post1->content, null);

        $this->assertEquals($post2->one, 1);
        $this->assertEquals($post2->id, null);
        $this->assertEquals($post2->title, null);
        $this->assertEquals($post2->slug, null);
        $this->assertEquals($post2->author_id, null);
        $this->assertEquals($post2->content, null);
    }
}