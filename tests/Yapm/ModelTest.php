<?php

use tests\Models\Post;

class ModelTest extends PHPUnit_Framework_TestCase {

    public function testConstructEmpty() {
        $post = new Post;
        $this->assertEquals($post->id, null);
        $this->assertEquals($post->title, null);
        $this->assertEquals($post->slug, null);
        $this->assertEquals($post->author_id, null);
        $this->assertEquals($post->content, null);
    }

    public function testConstructWithArgs() {
        $post = new Post(["title" => "The Start", "author_id" => 9, "slug" => "the-start", "content" => "All the things start with the beginning"]);
        $this->assertEquals($post->id, null);
        $this->assertEquals($post->title, "The Start");
        $this->assertEquals($post->slug, "the-start");
        $this->assertEquals($post->author_id, 9);
        $this->assertEquals($post->content, "All the things start with the beginning");
    }

    public function testPopulateExtras() {
        $post = new Post;
        $post->populate(["count" => 4], ["count"]);
        $this->assertEquals($post->id, null);
        $this->assertEquals($post->count, 4);
    }

    public function testPopulateExtrasV2() {
        $post = new Post;
        $post->populate(["count" => 4], false);
        $this->assertEquals($post->id, null);
        $this->assertEquals($post->count, 4);
    }

    public function testToArray() {
        $args = ["title" => "The Start", "author_id" => 9, "slug" => "the-start", "content" => "All the things start with the beginning"];
        $post = new Post($args);
        $args["id"] = null;
        $this->assertEquals($post->toArray(), $args);
    }
}