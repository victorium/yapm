<?php

namespace tests\Models;

use tests\Models\Contribution;
use tests\Models\Post;
use Yapm\Column;
use Yapm\Model;
use Yapm\Schema;

class Author extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "author", "id", [
            new Column\AutoIdType("id"),
            new Column\StringType("title", true),
            new Column\StringType("name"),
            new Column\OneToManyType("posts", Post::class),
            new Column\ManyToManyType("contributions", Contribution::class),
        ]);
    }
}