<?php

namespace tests\Models;

use tests\Models\Author;
use tests\Models\Contribution;
use Yapm\Column;
use Yapm\Model;
use Yapm\Schema;

class Post extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "post", "id", [
            new Column\AutoIdType("id"),
            new Column\StringType("title"),
            new Column\SlugType("slug", "title"),
            new Column\ForeignKeyType("author", Author::class),
            new Column\StringType("content"),
            new Column\ManyToManyType("contributors", Contribution::class),
        ]);
    }
}