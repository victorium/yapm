<?php

namespace tests\Models;

use tests\Models\Author;
use tests\Models\Post;
use Yapm\Column;
use Yapm\Model;
use Yapm\Schema;


class Contribution extends Model {
    protected static function defineSchema($config) {
        return new Schema($config, "contribution", "id", [
            new Column\AutoIdType("id"),
            new Column\ForeignKeyType("author", Author::class),
            new Column\ForeignKeyType("post", Post::class),
        ]);
    }
}